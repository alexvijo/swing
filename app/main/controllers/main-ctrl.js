'use strict';

swing.controller('TranslateController', function($translate, $scope) {
  $scope.changeLanguage = function (langKey) {
    $translate.use(langKey);
  };
});

swing.controller('HomeCtrl', ['userService','$scope', '$translate', function (userService, $scope, $translate, loginService) {

  $scope.changeLanguage = function (langKey) {
    $translate.use(langKey);
  };

  userService.getTaxis().then(function(data) {
    var taxis = data.taxis;
    $scope.taxis = taxis;
  });

  userService.getCursos().then(function(data) {
    var cursos = data.cursos;
    $scope.cursos = cursos;
  });

}]);

swing.controller('AvisoCtrl', function ($scope){
  var user = {}; 
  user.clases = [
    {
      "nombreClase":"LET'S TEACH Vol.1", 
      "imagenClase": "main/assets/images/logo-main.png", 
      "invitationalClase": true,
      "fechaClase": "12/07/2015", 
      "horaInicioClase": "18:00",
      "minutosClaseLeft":30, 
      "numClasesLeft": 4, 
      "localidadClase": "Poblenou"
    },
    {
      "nombreClase":"LET'S TEACH Vol.1", 
      "imagenClase": "main/assets/images/logo-main.png", 
      "invitationalClase": true,
      "fechaClase": "12/07/2015", 
      "horaInicioClase": "18:00",
      "minutosClaseLeft":30, 
      "numClasesLeft": 4, 
      "localidadClase": "Poblenou"
    }
  ];
  $scope.user = user;

})

swing.controller('TaxisCtrl', function (userService, $scope, $ionicPopup){

  $scope.showAlert = function() {
   var alertPopup = $ionicPopup.alert({
     title: '<div class="item-title-swing">TU SI QUE VALES {taxi.nombreTaxi}!!!<br>YA TE HEMOSAPUNTADO DE TAXI</div>',
     template: '<p>Te esperamos en 25 minutos en la sala 15 / SwingManuacs Gracia</p><p>Ves poniéndote las bambas y te vienes haciendo triple step :)</p>',
    scope: $scope,
    buttons: [
      {
        text: '<b>Save</b>',
        type: 'button-positive',
        onTap: function(e) {
          if (!$scope.data.wifi) {
            //don't allow the user to close unless he enters wifi password
            e.preventDefault();
          } else {
            return $scope.data.wifi;
          }
        }
      }
    ]

   });
  };

  userService.getTaxis().then(function(data) {
    var taxis = data.taxis;
    $scope.taxis = taxis;
  });

});

swing.controller('CursosCtrl', function (userService, $scope, $state, $ionicHistory){

  $scope.idCurso = $state.params.idCurso;

  $scope.myGoBack = function() {
    $ionicHistory.goBack();
  };

  userService.getCursos().then(function(data) {
    var cursos = data.cursos;
    $scope.cursos = cursos;
  });

});

swing.controller('CarnetCtrl', function (userService, $scope, $state, $ionicHistory){

  $scope.idEntrada = $state.params.idEntrada;

  $scope.myGoBack = function() {
    $ionicHistory.goBack();
  };

  userService.getUsuario().then(function(data) {
    var usuario = data.usuario;
    $scope.usuario = usuario;
  });

  userService.getEntradas().then(function(data) {
    var entradas = data.entradas;
    $scope.entradas = entradas;
  });

});

swing.controller('AjustesCtrl',  function ($scope, loginService){
 
  $scope.logout=function(){
    loginService.logout();
  };
});

swing.controller('LoginCtrl', function ($scope, loginService) {
  $scope.msgtxt='';
  $scope.login=function(data){
    loginService.login(data, $scope); 
    console.log('controller y data', data);
  };
});



/* SERVICES */
swing.factory('loginService', function ($http, $location, sessionService){
  return{
    login:function(data,scope){
      var $promise = $http.post('http://localhost/swing/app/user.php?callback=JSON_CALLBACK', data);
      $promise.then(function(msg){
        console.log('msg', msg);
        var uid = msg.data;
        console.log('uid (msg.data)', uid);
        if(uid){
          scope.msgtxt= 'Correct information';
          sessionService.set('uid',uid);
          $location.path('main/home');
        }        
        else  {
          scope.msgtxt= 'Email o contraseña incorrectos :(';
          $location.path('main/login');
        }          
      });
    },
    logout:function(){
      sessionService.destroy('uid');
      $location.path('main/login');
    },
    islogged:function(){
      var $checkSessionServer=$http.post('data/check_session.php');
      return $checkSessionServer;
      /*
      if(sessionService.get('user')) return true;
      else return false;
      */
    }
  }
});

swing.factory('sessionService', ['$http', function ($http){
  return{
    set:function(key,value){
      return sessionStorage.setItem(key,value);
    },
    get:function(key){
      return sessionStorage.getItem(key);
    },
    destroy:function(key){
      $http.post('data/destroy_session.php');
      return sessionStorage.removeItem(key);
    }
  };
}]);

swing.factory('userService', function($http) {
  var userService = {

    getCursos: function() {
      var promise = $http.get('user.json').then(function (response) {
        // console.log('response', response);
        return response.data;
      });
      return promise;
    },

    getTaxis: function() {
      var promise = $http.get('user.json').then(function (response) {
        // console.log('response', response);
        return response.data;
      });
      return promise;
    },

    getDiasRecuperacion: function() {
      var promise = $http.get('user.json').then(function (response) {
        return response.data;
      });
      return promise;
    },

    getEntradas: function() {
      var promise = $http.get('user.json').then(function (response) {
        // console.log('response', response);
        return response.data;
      });
      return promise;
    },

    getUsuario: function() {
      var promise = $http.get('user.json').then(function (response) {
        // console.log('response', response);
        return response.data;
      });
      return promise;
    }

  }
  return userService;
});