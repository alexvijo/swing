'use strict';

var swing = angular.module('main', [
  'ionic',
  'ngCordova',
  'ui.router',
  'pascalprecht.translate',
  'barcodeGenerator'
]);

swing.run(function ($rootScope, $location, loginService){
  var routespermission=['/home'];  //route that require login
  $rootScope.$on('$routeChangeStart', function(){
    if( routespermission.indexOf($location.path()) !=-1)
    {
      var connected=loginService.islogged();
      connected.then(function(msg){
        if(!msg.data) $location.path('admin/login');
      });
    }
  });
});

swing.config(function ($stateProvider, $ionicConfigProvider, $urlRouterProvider, $translateProvider) {

  $translateProvider.useStaticFilesLoader({
    prefix: '/languages/',
    suffix: '.json'
  });
  $translateProvider.preferredLanguage('es');
  //$translateProvider.useLocalStorage();
  // $translateProvider.determinePreferredLanguage();

  $ionicConfigProvider.views.maxCache(0);
  $urlRouterProvider.otherwise('/main/home');
  $ionicConfigProvider.tabs.position('bottom');
  
  // ROUTER
  $stateProvider
    .state('main', {
      url: '/main',
      abstract: true,
      templateUrl: 'main/templates/tabs.html'
    })

    .state('main.login', {
      url: '/login',
      views: {
        'login': {
          templateUrl: 'main/templates/login.html',
          controller : "LoginCtrl"
        }
      }
    })
    .state('main.home', {
      url: '/home',
      views: {
        'home': {
          templateUrl: 'main/templates/home.html',
          controller: 'HomeCtrl'
        }
      }
    })
    .state('main.aviso', {
      url: '/aviso',
      views: {
        'aviso': {
          templateUrl: 'main/templates/aviso.html',
          controller: 'AvisoCtrl'
        }
      }
    })
    .state('main.taxis', {
      cache: false,
      url: '/taxis',
      views: {
        'taxis': {
          templateUrl: 'main/templates/taxis.html',
          controller: 'TaxisCtrl'
        }
      }
    })

/* CURSOS */
    .state('main.cursos', {
      url: '/cursos',
      views: {
        'cursos': {
          templateUrl: 'main/templates/cursos.html',
          controller: 'CursosCtrl'
        }
      }
    })
    .state('main.cursoDetail', {
      url: '/cursos/detail/:idCurso',
      views: {
        'cursos': {
          templateUrl: 'main/templates/curso-detail.html',
          controller: 'CursosCtrl'
        }
      }
    })
    .state('main.cursoAvisarFaltaAsistencia', {
      url: '/cursos/clases/:idCurso',
      views: {
        'cursos': {
          templateUrl: 'main/templates/curso-avisar-falta-asistencia.html',
          controller: 'CursosCtrl'
        }
      }
    })    
    .state('main.avisarAsistencia', {
      url: '/cursos/avisar/:idCurso',
      views: {
        'cursos': {
          templateUrl: 'main/templates/avisar-falta-asistencia.html',
          controller: 'CursosCtrl'
        }
      }
    })


    .state('main.cursoGraciasPorAvisar', {
      url: '/cursos/gracias/:idCurso',
      cache: false,
      views: {
        'cursos': {
          templateUrl: 'main/templates/curso-gracias-por-avisar.html',
          controller: 'CursosCtrl'
        }
      }
    })
    .state('main.cursoRecuperacion', {
      url: '/cursos/recuperacion/:idCurso',
      cache: false,
      views: {
        'cursos': {
          templateUrl: 'main/templates/curso-seleccionar-dia-recuperacion.html',
          controller: 'CursosCtrl'
        }
      }
    })
/* CARNET / ENTRADAS */
    .state('main.carnet', {
      url: '/carnet',
      views: {
        'carnet': {
          templateUrl: 'main/templates/carnet.html',
          controller: 'CarnetCtrl'
        }
      }
    })
    .state('main.entradaDetail', {
      url: '/carnet/detail/:idEntrada',
      views: {
        'carnet': {
          templateUrl: 'main/templates/entrada-detail.html',
          controller: 'CarnetCtrl'
        }
      }
    })

    .state('main.ajustes', {
      cache: false,
      url: '/ajustes',
      views: {
        'ajustes': {
          templateUrl: 'main/templates/ajustes.html',
          controller: 'AjustesCtrl'
        }
      }
    });

    $urlRouterProvider.otherwise("main/login");
});


