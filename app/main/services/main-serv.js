'use strict';
var swing = angular.module('main');

swing.service('Main', function ($log, $timeout) {

  $log.log('Hello from your Service: Main in module main');

  // some initial data
  this.someData = {
    binding: 'Yes! Got that databinding working'
  };

  this.changeBriefly = function () {
    var initialValue = this.someData.binding;
    this.someData.binding = 'Yeah this was changed';

    var that = this;
    $timeout(function () {
      that.someData.binding = initialValue;
    }, 500);
  };

});

//SERVICES
swing.factory("registerUsers", function ($http, mensajesFlash){
    return {
        newRegister : function(user){
            return $http({
                //url: 'http://localhost:3000/#/main/user.json',
                method: "POST",
                data : "email="+user.email+"&password="+user.password, // +"&nombre="+user.nombre
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data){
                    if(data.respuesta == "success"){
                        mensajesFlash.clear();
                        mensajesFlash.show_success("El registro se ha procesado correctamente.");
                    }else if(data.respuesta == "exists"){
                        mensajesFlash.clear();
                        mensajesFlash.show_error("El email introducido ya existe en la bd.");
                    }else if(data.respuesta == "error_form"){
                        mensajesFlash.show_error("Ha ocurrido algún error al realizar el registro!.");
                    }
                }).error(function(){
                    mensajesFlash.show_error("Ha ocurrido algún error al realizar el registro!.");
                })
        }
    }
});

swing.factory("mensajesFlash", function ($rootScope){
    return {
        show_success : function(message){
            $rootScope.flash_success = message;
        },
        show_error : function(message){
            $rootScope.flash_error = message;
        },
        clear : function(){
            $rootScope.flash_success = "";
            $rootScope.flash_error = "";
        }
    }
});

swing.factory('userSevice', function ($timeout, $http) {
    var user = {
        fetch: function() {
            return $timeout(function() {
                return $http.get('user.json').then(function(response) {
                    return response.data;
                });
            }, 30);
        }
    }

    return user;
});
